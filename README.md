***
# Raspcar Mobile Part

![Raspcar Logo!](raspcar.png "RaspCar Logo")
***

## Groupe de TPT 1 - RaspCar - Participants

  -	Bah Mamadou Saliou
  
  -	Chatti Nader
  
  -	Diaz Gabriel
  
  -	Iben Salah Roukaya
  
  -	Ramoul Inès
  

# Prérequis

> - Installer ***Android Studio***
> - Installer ***Android SDK***
> - Installer ***Gradle***
> - Installer un ***émulateur*** ou utilser son ***smartphone Android***
> - Installer Git

# Installation
```sh
$ git clone https://gitlab.com/mbds/raspcar/raspcar-mobile-app.git app_mobile_name
```
# Ouverture du projet
```
Méthode 1 :
    * Lancer Android Studio
    * Ouvrir le projet cloné en tant que que Projet Android Studio

Méthode 2 :
	* Si vous disposez du tag NFC qui a été collé sur la boite du Raspberry Pi, il suffit de passer le smartphone Android dessus pour lancer l'installation

Méthode 3 :
    * Un dossier nommer "APK" contient le fichier apk servant à installer l'application

```
# Dépendances 
> - L'application mobile dépend d'une base de données **MongoDB** hébergé sur le cloud.
> - L'application mobile dépend aussi d'un Web Service développée en **Node Js**.
> - Il s'agit de la partie ***server*** du repos **RaspCar Web App**.
> - Ce serveur et l'appliaction mobile doivent être sur le même réseau local et avoir accès à internet.
> - On va récupérer **l'adresse IP de la machine sur laquelle le serveur tourne**:
```sh
Windows :
    $ ipconfig
Linux et Mac :
    $ ifconfig
```

# Configuration (Modification de certains fichiers)
```
L'adresse Ip de la machine récupré va nous permettre de Modifier les urls+port de tous les fichiers suivants :
    - app/src/main/java/com/mbds/raspcarv2/ui/main/networks/SocketManager.kt
    - app/src/main/java/com/mbds/raspcarv2/ui/main/networks/repositories/*
```

# Lancement de l'application
```
- Lancer son émulateur ou connecter son smartphone en mode débuggage
- Lancer l'application en cliquant sur la commande RUN
```