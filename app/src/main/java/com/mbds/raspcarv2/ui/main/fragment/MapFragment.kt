package com.mbds.raspcarv2.ui.main.fragment

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.github.nkzawa.emitter.Emitter
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.mbds.raspcarv2.R
import com.mbds.raspcarv2.ui.main.networks.SocketManager

class MapFragment : Fragment(), OnMapReadyCallback {

    lateinit var mapView: MapView
    lateinit var map: GoogleMap
    lateinit var sm: SocketManager
    private var marker: Marker? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_map, container, false)

        sm = SocketManager()
        mapView = root.findViewById(R.id.mapview)
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync(this)

        return root
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        if (googleMap != null) {
            map = googleMap
        };
        map.uiSettings.setAllGesturesEnabled(true)
        map.uiSettings.setMyLocationButtonEnabled(false)
        map.uiSettings.setZoomControlsEnabled(true)
        map.uiSettings.setCompassEnabled(true)
        map.uiSettings.setMapToolbarEnabled(true)
        getCarPostion()
    }


    override fun onResume() {
        mapView.onResume()
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    fun getCarPostion() {
        sm.onSocketReceive("CarPosition", ::onNewEvent)
    }

    private val onNewEvent = Emitter.Listener { args ->
        val position = args[0] as String
        val latLong = position.split(",").toTypedArray()
        val latitude = latLong[0]
        val longitude = latLong[1]
        Handler(Looper.getMainLooper()).post(Runnable {
            map.setOnMapLoadedCallback(OnMapLoadedCallback {
                val carPosition = LatLng(
                    java.lang.Double.valueOf(latitude),
                    java.lang.Double.valueOf(longitude)
                )
                marker = map.addMarker(
                    MarkerOptions()
                        .position(carPosition)
                        .title("Airpod")
                )
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(carPosition, 17f))
            })
        })
    }
}