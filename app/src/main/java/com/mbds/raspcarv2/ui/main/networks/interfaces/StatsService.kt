package com.mbds.raspcarv2.ui.main.networks.interfaces

import com.mbds.raspcarv2.ui.main.networks.responses.StatsResponse
import retrofit2.Call
import retrofit2.http.GET

interface StatsService {
    @GET("stats/")
    fun getStats(): Call<StatsResponse>
}