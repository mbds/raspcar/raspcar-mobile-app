package com.mbds.raspcarv2.ui.main.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.IMarker
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF
import com.mbds.raspcarv2.R
import com.mbds.raspcarv2.ui.main.networks.data.Stat
import com.mbds.raspcarv2.ui.main.networks.repositories.StatsRepository
import kotlinx.android.synthetic.main.marker_view.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.time.LocalDate
import kotlin.collections.ArrayList


class StatsFragment : Fragment() {

    lateinit var lineChart: LineChart
    lateinit var bc: BarChart
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_stats, container, false)

        bc = root.findViewById(R.id.barChart)
        lineChart=root.findViewById(R.id.lineChart)
        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        GlobalScope.launch {
            getData()
        }
    }

    //S'execute dans un thread secondeaire
    private suspend fun getData() {
        withContext(Dispatchers.IO) {
            val repository = StatsRepository()
            val res = repository.getStats()
            if (res.data.isNullOrEmpty()) {
                println(res)
                println(res.data)
            }
            bindData(res.data)
            setMarkerView()
        }
    }

    //S'execute sur le thread principal
    private suspend fun bindData(result: Array<Stat>) {
        withContext(Dispatchers.Main) {
            val stats = result
            val entriesBattery = ArrayList<BarEntry>()
            val entriesMileage = ArrayList<Entry>()
            val labels = ArrayList<String>()
            for (i in 0 until stats.size) {
                entriesBattery.add(BarEntry(i.toFloat(), stats[i].battery.toFloat()))
                entriesMileage.add(Entry(i.toFloat(), stats[i].mileage.toFloat()))
                var myDate = LocalDate.parse(stats[i].date.toString().substringBefore("T"))
                //Log.d("myDate is ==", myDate.toString())
                labels.add(myDate.toString())
                //Log.d("mileage :", stats[i].mileage.toString())
            }
            ///////////////////// BAR CHART //////////////////////////////
            // creating dataset for Bar Group
            val barDataSet = BarDataSet(entriesBattery, "Battery Capacity %")


            bc.getXAxis().setValueFormatter(IndexAxisValueFormatter(labels))
            val data = BarData(barDataSet)
            bc.getDescription().setText("Battery capacity consumed per day in %")
            bc.getDescription().setTextSize(10f)
            bc.setData(data)
            bc.xAxis.position = XAxis.XAxisPosition.TOP
            bc.xAxis.setGranularity(1f)
            bc.xAxis.setGranularityEnabled(true);
            bc.description.isEnabled = true
            bc.animateY(1000)
            bc.xAxis.setTextSize(10f)
            bc.legend.isEnabled = true
            // bc.setFitBars(true);
            bc.setPinchZoom(false)
            bc.xAxis.setDrawAxisLine(true)
            bc.xAxis.setDrawGridLines(false)
            bc.setDrawBarShadow(true)
            bc.data.setDrawValues(true)

            //////////////// LINE CHART ////////////////

            val vl = LineDataSet(entriesMileage, "Mileage per day")

            vl.setDrawValues(false)
            vl.setDrawFilled(true)
            vl.lineWidth = 3f
            vl.fillColor = R.color.gray
            vl.fillAlpha = R.color.red

            lineChart.xAxis.position = XAxis.XAxisPosition.TOP
            lineChart.xAxis.setGranularity(1f)
            lineChart.xAxis.setGranularityEnabled(true);
            lineChart.xAxis.setValueFormatter(IndexAxisValueFormatter(labels))
            lineChart.xAxis.labelRotationAngle = 0f

            lineChart.data = LineData(vl)

            lineChart.axisRight.isEnabled = false

            lineChart.setTouchEnabled(true)
            lineChart.setPinchZoom(true)

            lineChart.getDescription().setText("Distance traveled per day in km")
            lineChart.setNoDataText("No data yet!")
            lineChart.getDescription().setTextSize(10f)


            lineChart.animateX(1800, Easing.EaseInExpo)


        }
    }

    private fun setMarkerView(){
        //init marker
        val markerView = CustomMarker(this.requireContext(), R.layout.marker_view)
        lineChart.marker = markerView
    }

}

//etiquette de la valeur de l'ordonnee du point
class CustomMarker(context: Context, layoutResource: Int):  MarkerView(context, layoutResource),
    IMarker {
    override fun refreshContent(entry: Entry?, highlight: Highlight?) {
        val value = entry?.y?.toDouble() ?: 0.0
        var resText = ""
        if(value.toString().length > 8){
            resText = value.toString().substring(0,7) + " km "
        }
        else{
            resText = value.toString() + " km "
        }
        tvVal.text = resText
        super.refreshContent(entry, highlight)
    }

    override fun getOffsetForDrawingAtPoint(xpos: Float, ypos: Float): MPPointF {
        return MPPointF(-width / 2f, -height - 10f)
    }
}

