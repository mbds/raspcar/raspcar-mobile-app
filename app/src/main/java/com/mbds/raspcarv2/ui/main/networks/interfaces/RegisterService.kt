package com.mbds.raspcarv2.ui.main.networks.interfaces

import com.mbds.raspcarv2.ui.main.fragment.User
import com.mbds.raspcarv2.ui.main.networks.responses.UserResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface RegisterService {
    @POST("register/")
    fun setRegister(@Body user: User): Call<UserResponse>
}