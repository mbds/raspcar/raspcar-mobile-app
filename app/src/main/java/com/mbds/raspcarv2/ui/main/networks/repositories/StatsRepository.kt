package com.mbds.raspcarv2.ui.main.networks.repositories

import android.util.Log
import com.mbds.raspcarv2.ui.main.networks.responses.StatsResponse
import com.mbds.raspcarv2.ui.main.networks.interfaces.StatsService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class StatsRepository {
    private val service: StatsService
    init {
        val retrofit = Retrofit.Builder().apply {
            baseUrl("http://192.168.1.11:8080/")
                .addConverterFactory(GsonConverterFactory.create())}.build()
        service = retrofit.create(StatsService::class.java)
    }

    fun getStats(): StatsResponse {
        val response= service.getStats().execute()
        Log.d("Response JSON",response.toString())
        return response.body() ?: StatsResponse(
            false,
            true,
            "empty msg",
            emptyArray()
        )
    }

}