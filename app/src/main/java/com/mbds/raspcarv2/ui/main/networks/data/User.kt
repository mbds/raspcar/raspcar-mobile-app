package com.mbds.raspcarv2.ui.main.fragment

import android.text.LoginFilter
import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("userName") val userName: String,
    @SerializedName("password") val password: String
)
