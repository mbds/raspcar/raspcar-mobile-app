package com.mbds.raspcarv2.ui.main.networks.interfaces

import com.mbds.raspcarv2.ui.main.fragment.User
import com.mbds.raspcarv2.ui.main.networks.responses.UserResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface UpdateUserService {
    @POST("update/user/")
    fun setUpdateUser(@Body user: User): Call<UserResponse>
}