package com.mbds.raspcarv2.ui.main.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.mbds.raspcarv2.R
import com.mbds.raspcarv2.ui.main.fragment.ControlsFragment
import com.mbds.raspcarv2.ui.main.fragment.StatsFragment
import com.mbds.raspcarv2.ui.main.fragment.MapFragment
import com.mbds.raspcarv2.ui.main.fragment.UserFragment

private val TAB_TITLES = arrayOf(
        R.string.tab_controls,
        R.string.tab_stats,
        R.string.tab_map,
        R.string.tab_user
    )

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(private val context: Context, fm: FragmentManager)
    : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).

        var fragment: Fragment = StatsFragment()

        when (position) {
            0 ->  fragment = ControlsFragment()
            1 ->  fragment = StatsFragment()
            2 -> fragment = MapFragment()
            3 -> fragment = UserFragment()
        }
        return fragment;
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        return 4
    }
}