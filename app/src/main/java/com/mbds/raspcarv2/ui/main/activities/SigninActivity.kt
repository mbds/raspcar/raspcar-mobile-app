package com.mbds.raspcarv2.ui.main.activities

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.mbds.raspcarv2.R
import com.mbds.raspcarv2.ui.main.fragment.User
import com.mbds.raspcarv2.ui.main.networks.repositories.RegisterRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SigninActivity : AppCompatActivity() {

    lateinit var user: EditText
    lateinit var password: EditText
    lateinit var info: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)

        user = findViewById(R.id.userNameEditSignin)
        password = findViewById(R.id.userPasswordEditSignin)
        var passwordVisible = findViewById<ImageView>(R.id.passwordVisibilitySignin)
        passwordVisible.isVisible = false
        var passwordInvisible = findViewById<ImageView>(R.id.passwordInvisibilitySignin)
        var labelPasswordVal = findViewById<TextView>(R.id.userPasswordValidateSignin)
        labelPasswordVal.isVisible = false
        var passwordVal = findViewById<EditText>(R.id.userPasswordValidateEditSignin)
        passwordVal.isVisible = false
        var passwordValVisible = findViewById<ImageView>(R.id.passwordValVisibilitySignin)
        passwordValVisible.isVisible = false
        var passwordValInvisible = findViewById<ImageView>(R.id.passwordValInvisibilitySignin)
        passwordValInvisible.isVisible = false
        var saveBtn = findViewById<Button>(R.id.submitSignin)
        saveBtn.isEnabled = false
        var cancelBtn = findViewById<Button>(R.id.cancelSignin)
        info = findViewById(R.id.infoSignin)

        // When user fill the password he must have 5 charactere minimum
        // else display an error message
        // If all field are correctly filled, enable the save button
        user.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                if (s.length < 5) {
                    info.setTextColor(Color.parseColor("#FB0B0B"))
                    info.text = "Username must contain at least 5 characters"
                } else {
                    info.text = ""
                }

                saveBtn.isEnabled = false
                if (user.text.length >= 5 && password.text.length >= 10 && passwordVal.text.toString() == password.text.toString() && info.text == "") {
                    saveBtn.isEnabled = true
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {}
        })

        // When user fill the password he must have 10 charactere minimum
        // else display an error message
        // if it's ok, display an other editText field for validate the password
        // If all field are correctly filled, enable the save button
        password.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {

                if (s.length < 10) {
                    info.setTextColor(Color.parseColor("#FB0B0B"))
                    info.text = "Password must contain at least 10 characters"
                } else {
                    info.text = ""
                    labelPasswordVal.isVisible = true
                    passwordVal.isVisible = true
                    passwordValInvisible.isVisible = true
                }

                if (password.text.toString() != passwordVal.text.toString() && passwordVal.isVisible) {
                    info.setTextColor(Color.parseColor("#FB0B0B"))
                    info.text = "The two passwords are not identical"
                }

                saveBtn.isEnabled = false
                if (user.text.length >= 5 && password.text.length >= 10 && passwordVal.text.toString() == password.text.toString() && info.text == "") {
                    saveBtn.isEnabled = true
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {}
        })

        // Verify if the two passwords are equal
        // If all field are correctly filled, enable the save button
        passwordVal.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                if (password.text.toString() != passwordVal.text.toString()) {
                    info.setTextColor(Color.parseColor("#FB0B0B"))
                    info.text = "The two passwords are not identical"
                } else {
                    info.text = ""
                }

                saveBtn.isEnabled = false
                if (user.text.length >= 5 && password.text.length >= 10 && passwordVal.text.toString() == password.text.toString() && info.text == "") {
                    saveBtn.isEnabled = true
                }

            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {}
        })

        // If button is enabled we save the profil
        // else display an error message relative to the reason of disabled
        saveBtn.setOnClickListener() {
            if (saveBtn.isEnabled) {
                GlobalScope.launch {
                    saveAccount()
                }
            }
        }

        cancelBtn.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }

        passwordInvisible.setOnClickListener() {
            password.transformationMethod = HideReturnsTransformationMethod.getInstance()
            passwordInvisible.isVisible = false
            passwordVisible.isVisible = true
        }

        passwordVisible.setOnClickListener() {
            password.transformationMethod = PasswordTransformationMethod.getInstance()
            passwordInvisible.isVisible = true
            passwordVisible.isVisible = false
        }

        passwordValInvisible.setOnClickListener() {
            passwordVal.transformationMethod = HideReturnsTransformationMethod.getInstance()
            passwordValInvisible.isVisible = false
            passwordValVisible.isVisible = true
        }

        passwordValVisible.setOnClickListener() {
            passwordVal.transformationMethod = PasswordTransformationMethod.getInstance()
            passwordValInvisible.isVisible = true
            passwordValVisible.isVisible = false
        }

    }

    // Run on a secondary thread
    private suspend fun saveAccount() {
        withContext(Dispatchers.IO) {
            val repository = RegisterRepository()
            var account: User = User(userName = user.text.toString(), password = password.text.toString())
            val res = repository.setRegister(account)

            displayMsg(res.succes, res.msg)
        }
    }

    // run on the main thread
    private suspend fun displayMsg(accountSaved: Boolean, ErrMsg: String) {
        withContext(Dispatchers.Main.immediate) {
            if (accountSaved) {
                // Display the positive message in green
                info.setTextColor(Color.parseColor("#33FA29"))
                info.text = "Profile successfully created"
            } else if (ErrMsg == "l'utilsateur existe déjà") {
                info.setTextColor(Color.parseColor("#FB0B0B"))
                info.text = "Username is not available"
            } else {
                info.setTextColor(Color.parseColor("#FB0B0B"))
                info.text = "An error has occurred, please try again later"
            }
        }
    }

}