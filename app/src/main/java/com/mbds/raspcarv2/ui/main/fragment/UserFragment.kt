package com.mbds.raspcarv2.ui.main.fragment

import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.mbds.raspcarv2.R
import com.mbds.raspcarv2.ui.main.networks.repositories.UpdateUserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.*

class UserFragment : Fragment(){

    lateinit var user: EditText
    lateinit var password: EditText
    lateinit var info: TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_user, container, false)

        user = root.findViewById<EditText>(R.id.userNameEditManager)
        // Here the username is on read only mode
        user.isEnabled = false
        user.isFocusable = false
        password = root.findViewById<EditText>(R.id.userPasswordEditManager)
        var passwordVisible = root.findViewById<ImageView>(R.id.passwordVisibilityManager)
        passwordVisible.isVisible = false
        var passwordInvisible = root.findViewById<ImageView>(R.id.passwordInvisibilityManager)
        var saveBtn = root.findViewById<Button>(R.id.submitManager)
        info = root.findViewById<TextView>(R.id.infoManager)


        // Read on the files systems wich create on the login activity to have the current username/password
        val fileUser: FileInputStream = getActivity()!!.openFileInput("username")
        var inputUser = InputStreamReader(fileUser)
        val bufferedUser = BufferedReader(inputUser)
        val stringUser: StringBuilder = StringBuilder()
        var textUser: String? = null
        while ({ textUser = bufferedUser.readLine(); textUser }() != null) {
            stringUser.append(textUser)
        }
        user.setText(stringUser.toString()).toString()

        val filePassword: FileInputStream = getActivity()!!.openFileInput("password")
        var inputPassword = InputStreamReader(filePassword)
        val bufferedPassword = BufferedReader(inputPassword)
        val stringPassword: StringBuilder = StringBuilder()
        var textPassword: String? = null
        while ({ textPassword = bufferedPassword.readLine(); textPassword }() != null) {
            stringPassword.append(textPassword)
        }
        password.setText(stringPassword.toString()).toString()

        if (password.text.length < 10) {
            saveBtn.isEnabled = false
            info.setTextColor(Color.parseColor("#FB0B0B"))
            info.text = "Password must contain at least 10 characters"
        }

        password.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                if (s.length < 10) {
                    info.setTextColor(Color.parseColor("#FB0B0B"))
                    info.text = "Password must contain at least 10 characters"
                } else {
                    info.text = ""
                }

                saveBtn.isEnabled = false
                if (user.text.length >= 5 && password.text.length >= 10 && info.text == "") {
                    saveBtn.isEnabled = true
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {}
        })

        passwordInvisible.setOnClickListener() {
            password.transformationMethod = HideReturnsTransformationMethod.getInstance()
            passwordInvisible.isVisible = false
            passwordVisible.isVisible = true
        }

        passwordVisible.setOnClickListener() {
            password.transformationMethod = PasswordTransformationMethod.getInstance()
            passwordInvisible.isVisible = true
            passwordVisible.isVisible = false
        }

        // Add a listener on the save button
        // For now, no db connection so just display message
        saveBtn.setOnClickListener {
            GlobalScope.launch {
                saveAccount()
            }
        }

        return root
    }

    // Run on a secondary thread
    private suspend fun saveAccount() {
        withContext(Dispatchers.IO) {
            val repository = UpdateUserRepository()
            var account: User = User(userName = user.text.toString(), password = password.text.toString())
            val res = repository.setUpdateUser(account)

            displayMsg(res.succes)

        }
    }

    // run on the main thread
    private suspend fun displayMsg(accountSaved: Boolean) {
        withContext(Dispatchers.Main.immediate) {
            if (accountSaved) {
                // Display the positive message in green
                info.setTextColor(Color.parseColor("#33FA29"))
                info.text = "update completed"
            } else {
                info.setTextColor(Color.parseColor("#FB0B0B"))
                info.text = "An error has occurred, please try again later"
            }
        }
    }

}