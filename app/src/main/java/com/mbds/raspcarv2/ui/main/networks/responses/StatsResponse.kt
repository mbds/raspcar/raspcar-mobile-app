package com.mbds.raspcarv2.ui.main.networks.responses

import com.mbds.raspcarv2.ui.main.networks.data.Stat

class StatsResponse(val success : Boolean, val error : Boolean, val msg: String, val data : Array<Stat>){
}