package com.mbds.raspcarv2.ui.main.activities

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.mbds.raspcarv2.R
import com.mbds.raspcarv2.ui.main.fragment.User
import com.mbds.raspcarv2.ui.main.networks.repositories.LoginRepository
import kotlinx.coroutines.*
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException

class LoginActivity : AppCompatActivity() {

    lateinit var user: EditText
    lateinit var password: EditText
    lateinit var loginInfo1: TextView
    lateinit var loginInfo2: TextView

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        user = findViewById(R.id.userNameEditLogin)
        password = findViewById(R.id.userPasswordEditLogin)
        var passwordVisible = findViewById<ImageView>(R.id.passwordVisibilityLogin)
        passwordVisible.isVisible = false
        var passwordInvisible = findViewById<ImageView>(R.id.passwordInvisibilityLogin)
        var loginBtn = findViewById<Button>(R.id.Login_button)
        // Enabled if all field are empty
        loginBtn.isEnabled = false
        var signinLink = findViewById<TextView>(R.id.signin)
        // Undeline the signin label to simulate a hyperlink
        signinLink.paintFlags = Paint.UNDERLINE_TEXT_FLAG
        loginInfo1 = findViewById<TextView>(R.id.infoLogin1)
        loginInfo2 = findViewById<TextView>(R.id.infoLogin2)

        // If all field are filled, enable the save button
        user.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                loginBtn.isEnabled = false
                if (user.text.toString() != "" && password.text.toString() != "") {
                    loginBtn.isEnabled = true
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {}
        })

        password.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                loginBtn.isEnabled = false
                if (user.text.toString() != "" && password.text.toString() != "") {
                    loginBtn.isEnabled = true
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {}
        })

        passwordInvisible.setOnClickListener() {
            password.transformationMethod = HideReturnsTransformationMethod.getInstance()
            passwordInvisible.isVisible = false
            passwordVisible.isVisible = true
        }

        passwordVisible.setOnClickListener() {
            password.transformationMethod = PasswordTransformationMethod.getInstance()
            passwordInvisible.isVisible = true
            passwordVisible.isVisible = false
        }

        // Add a listener to the login button
        // If the login/password are find in Database, the system go to the home page
        // Else display an error message
        loginBtn.setOnClickListener {

            GlobalScope.launch {
                verifyAcount()
            }

        }

        // Add a OnClickListner to the signin TextView
        // The idea is to simulate a link as many mobile application
        // When click, system go on the signin page
        signinLink.setOnClickListener {
            val intent = Intent(this, SigninActivity::class.java)
            startActivity(intent)
        }
    }

    // Run on a secondary thread
    private suspend fun verifyAcount() {
        withContext(Dispatchers.IO) {
            val repository = LoginRepository()
            var authentification: User = User(userName = user.text.toString(), password = password.text.toString())
            val res = repository.setLogin(authentification)

            launchApp(res.succes)
        }
    }

    // run on the main thread
    private suspend fun launchApp(accountVerrified: Boolean) {
        withContext(Dispatchers.Main.immediate) {
            if (accountVerrified) {
                loginInfo1.text = ""
                loginInfo2.text = ""

                // We need to keep which user is connected
                // Create two privates files : username and password
                val fileUser: FileOutputStream
                val filePassword: FileOutputStream
                try {
                    fileUser = openFileOutput("username", Context.MODE_PRIVATE)
                    fileUser.write(user.text.toString().toByteArray())
                    filePassword = openFileOutput("password", Context.MODE_PRIVATE)
                    filePassword.write(password.text.toString().toByteArray())
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                } catch (e: NumberFormatException) {
                    e.printStackTrace()
                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                val intent = Intent(this@LoginActivity, MainActivity::class.java)
                startActivity(intent)
            } else {
                loginInfo1.text = "Connection refused"
                loginInfo2.text = "Username/Password incorrect"
            }
        }
    }




}