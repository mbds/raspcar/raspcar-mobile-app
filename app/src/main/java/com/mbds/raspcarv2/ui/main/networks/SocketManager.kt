package com.mbds.raspcarv2.ui.main.networks

import com.github.nkzawa.emitter.Emitter
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import java.net.URISyntaxException

class SocketManager {

    val SOCKET_SERVER_URL = "http://192.168.43.24:3000"
    var mSocket: Socket

    init {
        try {
            mSocket = IO.socket(SOCKET_SERVER_URL)
            mSocket.connect()
            sendSocket("NewConnectedDevice", android.os.Build.MANUFACTURER)

        } catch (e: URISyntaxException) {
            throw e
        }
    }

    fun onSocketReceive(eventId: String, callBackFunction: () -> Emitter.Listener) {
        mSocket.on(eventId, callBackFunction())
    }

    fun sendSocket(eventId: String, data: Any) {
        mSocket.emit(eventId, data)
    }
}