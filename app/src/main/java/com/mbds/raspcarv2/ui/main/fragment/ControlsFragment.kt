package com.mbds.raspcarv2.ui.main.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.mbds.raspcarv2.R
import com.mbds.raspcarv2.ui.main.networks.SocketManager


class ControlsFragment : Fragment() {

    var state_lock = true
    var state_start = false
    var state_light = false
    lateinit var sm: SocketManager


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_controls, container, false)
        val lock: ImageButton = root.findViewById(R.id.home_lock)
        val start: ImageButton = root.findViewById(R.id.home_start)
        val light: ImageButton = root.findViewById(R.id.home_light)
        val klaxon: ImageButton = root.findViewById(R.id.home_control_horn)
        sm = SocketManager()

        lock.setOnClickListener {
            if (state_lock) {
                state_lock = false
                lock.setImageResource(R.drawable.ic_unlock)
                //snack bar
                val snackBar = Snackbar.make(
                    activity!!.findViewById(android.R.id.content),
                    "The car is unlocked", Snackbar.LENGTH_LONG
                )
                snackBar.show()
            } else {
                state_lock = true
                lock.setImageResource(R.drawable.ic_lock)
                //snack bar
                val snackBar = Snackbar.make(
                    activity!!.findViewById(android.R.id.content),
                    "The car is locked", Snackbar.LENGTH_LONG
                )
                snackBar.show()
            }
        }

        start.setOnClickListener {
            if (state_start) {
                sm.sendSocket("state_start", state_start)
                state_start = false
                start.setImageResource(R.drawable.ic_start)
                //snack bar
                val snackBar = Snackbar.make(
                    activity!!.findViewById(android.R.id.content),
                    "The car is started", Snackbar.LENGTH_LONG
                )
                snackBar.show()
            } else {
                //sm.sendSocket("state_start", state_start)
                state_start = true
                start.setImageResource(R.drawable.ic_stop)
                //snack bar
                val snackBar = Snackbar.make(
                    activity!!.findViewById(android.R.id.content),
                    "The car is stopped", Snackbar.LENGTH_LONG
                )
                snackBar.show()
            }
        }

        klaxon.setOnClickListener {

            klaxon.setImageResource(R.drawable.horn)
            //snack bar
            val snackBar = Snackbar.make(
                activity!!.findViewById(android.R.id.content),
                "pip pip pipppppp", Snackbar.LENGTH_LONG
            )
            snackBar.show()
        }

        light.setOnClickListener {
            if (state_light) {
                //sm.sendSocket("state_start", state_start)
                state_light = false
                light.setImageResource(R.drawable.ic_light_off)
                //snack bar
                val snackBar = Snackbar.make(
                    activity!!.findViewById(android.R.id.content),
                    "The light is stopped", Snackbar.LENGTH_LONG
                )
                snackBar.show()
            } else {
                //sm.sendSocket("state_start", state_start)
                state_light = true
                light.setImageResource(R.drawable.ic_light_on)
                //snack bar
                val snackBar = Snackbar.make(
                    activity!!.findViewById(android.R.id.content),
                    "The light is started", Snackbar.LENGTH_LONG
                )
                snackBar.show()
            }
        }
        return root
    }
}