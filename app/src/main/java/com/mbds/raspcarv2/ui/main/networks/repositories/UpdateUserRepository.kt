package com.mbds.raspcarv2.ui.main.networks.repositories

import android.util.Log
import com.mbds.raspcarv2.ui.main.fragment.User
import com.mbds.raspcarv2.ui.main.networks.interfaces.UpdateUserService
import com.mbds.raspcarv2.ui.main.networks.responses.UserResponse
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class UpdateUserRepository {
    private val service: UpdateUserService
    init {
        val retrofit = Retrofit.Builder().apply {
            baseUrl("http://192.168.43.24:8082/")
                .addConverterFactory(GsonConverterFactory.create())}.build()
        service = retrofit.create(UpdateUserService::class.java)
    }

    fun setUpdateUser(user: User): UserResponse {
        val response= service.setUpdateUser(user).execute()
        Log.d("Response JSON",response.toString())
        return response.body() ?: UserResponse(
            false,
            true,
            "empty msg",
            emptyArray()
        )
    }

}