package com.mbds.raspcarv2.ui.main.networks.responses


import com.mbds.raspcarv2.ui.main.fragment.User

class UserResponse(val succes : Boolean, val error : Boolean, val msg: String, val data : Array<User>){
}