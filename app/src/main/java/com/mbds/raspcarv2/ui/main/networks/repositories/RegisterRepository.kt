package com.mbds.raspcarv2.ui.main.networks.repositories

import android.util.Log
import com.mbds.raspcarv2.ui.main.fragment.User
import com.mbds.raspcarv2.ui.main.networks.interfaces.RegisterService
import com.mbds.raspcarv2.ui.main.networks.responses.UserResponse
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class RegisterRepository {
    private val service: RegisterService
    init {
        val retrofit = Retrofit.Builder().apply {
            baseUrl("http://192.168.43.24:8082/")
                .addConverterFactory(GsonConverterFactory.create())}.build()
        service = retrofit.create(RegisterService::class.java)
    }

    fun setRegister(user: User): UserResponse {
        val response= service.setRegister(user).execute()
        Log.d("Response JSON",response.toString())
        return response.body() ?: UserResponse(
            false,
            true,
            "empty msg",
            emptyArray()
        )
    }

}