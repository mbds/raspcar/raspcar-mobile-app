package com.mbds.raspcarv2.ui.main.networks.data

import com.google.gson.annotations.SerializedName
import java.util.*

data class Stat(
    @SerializedName("date") val date: String,
    @SerializedName("mileage") val mileage : Int,
    @SerializedName("battery") val battery: Int
)

