package com.mbds.raspcarv2

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.mbds.raspcarv2.ui.main.networks.interfaces.StatsService
import com.mbds.raspcarv2.ui.main.networks.repositories.StatsRepository
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Before
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class StatTest {

    private lateinit var server: MockWebServer
    private lateinit var retrofit: Retrofit
    private lateinit var service: StatsService

    @Before
    fun setup() {
        server = MockWebServer()
        retrofit = Retrofit.Builder().apply {
            baseUrl("http://192.168.1.11:8080/")
                .addConverterFactory(GsonConverterFactory.create())}.build()
        service = retrofit.create(StatsService::class.java)
    }

    @After
    fun teardown() {
        server.close()
    }

    @Test
    fun StatsAPITest() = runBlocking<Unit> {

        val response = service.getStats().execute().body()

    //message displayed only when an assert fails
        assertFalse("StatsTest : response is null", response == null)
        if (response != null) {
            assertTrue("StatsTest: response success set to false" ,response.success)
            assertEquals("StatsTest : check size of data returned" ,response.data.size, 10)
        }

    }

}